
import java.util.*;
import java.util.stream.Collectors;

// meetod getTokensFromString inspireeritud allikast: https://www.baeldung.com/java-stringtokenizer

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      setName(n);
      setFirstChild(d);
      setRightSibling(r);
   }

   Node (String n) {
      setName(n);
      setFirstChild(null);
      setRightSibling(null);
   }

   Node() {
      setName("");
      setFirstChild(null);
      setRightSibling(null);
   }

   private  void setRightSibling(Node r) {
      nextSibling = r;
   }

   private void setFirstChild(Node d) {
      firstChild = d;
   }

   private  void setName(String n) {
      name = n;
   }

   public String getName() {
      return name;
   }

   public  Node getFirstChild() {
      return firstChild;
   }

   public Node getRightSibling() {
      return nextSibling;
   }

   @Override
   public String toString() {
      return leftParentheticRepresentation();
   }

   public String leftParentheticRepresentation() {
      StringBuilder str = new StringBuilder();
      str.append(this.getName());
      Node children = this.getFirstChild();
      boolean addParenthesis = true;

      while (children != null) {
         if (addParenthesis) {
            str.append("(");
         }
         str.append((children).leftParentheticRepresentation());
         children = children.getRightSibling();
         if (children == null) {
            str.append(")");
         } else {
            str.append(",");
            addParenthesis = false;
         }
      }
      return str.toString();
   }

   public String pseudoXmlRepresentation(int depth) {
      if (this.getName().equals("")) {
         return "";
      }
      StringBuilder result = new StringBuilder();
      for (int i = 0; i < depth-1; i++) {
         result.append("\t");
      }
      result.append("<L").append(depth).append("> ");
      result.append(this.getName()).append(" ");
      Node children = this.getFirstChild();
      boolean addTabs = false;

      while (children != null) {
         result.append("\n");
         result.append((children).pseudoXmlRepresentation(depth+1));
         children = children.getRightSibling();
         if (children == null) {
            result.append("\n");
            addTabs = true;
         }
      }
      if (addTabs) {
         for (int i = 0; i < depth-1; i++) {
            result.append("\t");
         }
      }
      result.append("</L").append(depth).append(">");

      return result.toString();
   }

   public void addChild (Node a) {
      if (a == null) {return;}
      Node children = this.getFirstChild();
      if (children == null) {
         this.setFirstChild(a);
      } else {
         while (children.getRightSibling() != null) {
            children = children.getRightSibling();
         }
         (children).setRightSibling(a);
      }
   }

   public static Node parsePostfix (String s) {

      validateInputString(s);

      if (validateParenthesis(s)) return new Node(s);

      List<String> tokens = getTokensFromString(s, "(,)");
      ArrayList<String> specialSymbols = new ArrayList<>(Arrays.asList("(", ",", ")"));
      Stack<Node> stack = new Stack<>();

      for (int i = 0; i < tokens.size(); i++) {
         String currentToken = tokens.get(i);
         if (currentToken.equals("(")) {
            stack.push(new Node());
         } else if (!specialSymbols.contains(currentToken) && i != tokens.size() - 1) {
           stack.lastElement().addChild(new Node(currentToken));
         } else if (currentToken.equals(")")) {
            stack.lastElement().setName(tokens.get(i+1));
            if (i != tokens.size() - 2) {
               Node newChild = stack.pop();
               stack.lastElement().addChild(newChild);
            }
            i++; // SKIP THE TOKEN AFTER ")"
         }
      }
      return stack.pop();
   }


   public static List<String> getTokensFromString(String s, String del) {
      return Collections.list(new StringTokenizer(s, del, true)).stream()
              .map(token -> (String) token)
              .collect(Collectors.toList());
   }


   private static boolean validateParenthesis(String s) {
      List<String> tokens = getTokensFromString(s, "(,)");

      int leftParenthesisCount = 0;
      int rightParenthesisCount = 0;
      for (String token : tokens) {
         if (token.equals("(")) {
            leftParenthesisCount++;
         } else if (token.equals(")")) {
            rightParenthesisCount++;
         }
      }

      if (leftParenthesisCount != rightParenthesisCount) {
         throw new RuntimeException("Input postfix string is invalid!");
      } else if (leftParenthesisCount == 0 && !s.contains(",")) {
         return true;
      }
      return false;
   }


   private static void validateInputString(String s) {
      ArrayList<String> illegalCombinations = new ArrayList<>(Arrays.asList(" ", ",,", "()", "(,", "),", "\t"));
      for (String combination : illegalCombinations) {
         if (s.contains(combination)) {
            throw new RuntimeException("Input postfix string is invalid!");
         }
      }
      if (s.contains("((") && s.contains("))")) {
         throw new RuntimeException("Input postfix string is invalid!");
      }
   }


   public static void main (String[] param) {

      String s = "";
      Node t = Node.parsePostfix(s);
//      System.out.println("result: " + t.getName());
      System.out.println(t.leftParentheticRepresentation());
      System.out.println();
      System.out.println(t.pseudoXmlRepresentation(1)); // parameter "1" is required for correct representation
//
//      System.out.println("Prefix: " + t.toString());

//      String k = "(B,(D,E)C,(G,(I,J)H)F)A";
//      Node h = Node.parsePostfix(k);
//      System.out.println("To solve: " + k);
//      System.out.println("Expected: A(B,C(D,E),F(G,H(I,J)))");
//      System.out.println("Actual  : " + h.toString());

//      String k = "(((2,1)-,4)*,(6,3)/)+";
//      Node h = Node.parsePostfix(k);
//      System.out.println("To solve: " + k);
//      System.out.println("Expected: +(*(-(2,1),4),/(6,3))");
//      System.out.println("Actual  : " + h.toString());

//      String k = "(B,,C)A";
//      Node h = Node.parsePostfix(k);
//      System.out.println("To solve: " + k);
//      System.out.println("Expected: ");
//      System.out.println("Actual  : " + h.toString());
//
//       String s = "(B1,C)A";
//
//      test1(s);
//      System.out.println(t.toString());
//      String v = t.leftParentheticRepresentation();
//      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}

